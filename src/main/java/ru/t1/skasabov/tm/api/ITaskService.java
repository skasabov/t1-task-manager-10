package ru.t1.skasabov.tm.api;

import ru.t1.skasabov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
