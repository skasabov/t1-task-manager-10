package ru.t1.skasabov.tm.api;

import ru.t1.skasabov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

}
