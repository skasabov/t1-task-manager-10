package ru.t1.skasabov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
