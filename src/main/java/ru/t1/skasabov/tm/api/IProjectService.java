package ru.t1.skasabov.tm.api;

import ru.t1.skasabov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
