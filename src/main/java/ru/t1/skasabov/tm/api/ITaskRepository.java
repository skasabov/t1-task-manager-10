package ru.t1.skasabov.tm.api;

import ru.t1.skasabov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

}
