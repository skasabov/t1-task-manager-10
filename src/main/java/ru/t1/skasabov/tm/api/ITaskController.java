package ru.t1.skasabov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
